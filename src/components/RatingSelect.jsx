import { useState, useContext, useEffect } from 'react'
import FeedbackContext from '../context/FeedbackContext';

function RatingSelect({select}) {
  const [selected, setSelected] = useState(10)
  const { feedbackEdit } = useContext(FeedbackContext)

  useEffect(() => {
    setSelected(feedbackEdit.item.rating)
  }, [feedbackEdit])
  
  const numbers = Array.from({ length: 10 }, (_, index) => index + 1);

  const handleChange = (e) => { 
    setSelected(+e.currentTarget.value)
    select(+e.currentTarget.value)
  }
  return (
    <ul className='rating'>
      {numbers.map((n) => (
        <li key={n}>
          <input 
            type='radio'
            id={`num${n}`}
            name='rating'
            value={n}
            onChange={handleChange}
            checked={selected === n}
          />
          <label htmlFor={`num${n}`}>{n}</label>
        </li>
      ))}
      
    </ul>
  )
}

export default RatingSelect